Name:           rpm-repo-manager
Version:        0.1.1
Release:        1%{?dist}
Summary:        A simple RPM repo manager
BuildArch:      noarch

License:        GPLv3+
URL:            https://gitlab.com/jlisher/%{name}/

Source0:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz
Source1:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.asc
Source2:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum
Source3:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum.asc
Source4:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg
Source5:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg.sha256sum

BuildRequires:  gnupg2
Requires:       createrepo_c
Requires:       (coreutils or coreutils-single)

%description
A simple RPM repo manager

%prep
# make sure everything runs successfully
set -euo pipefail

# Using `head -c 64` for checksums, as a sha256sum is 64 characters
# Checksums are not perfect, but doesn't hurt, too much

# Verify GPG keyring checksum (consistency)
[[ "$(sha256sum %{SOURCE4} | head -c 64)" == "$(head -c 64 %{SOURCE5})" ]] || {
  echo "Failed to verify the GPG keyring checksum"
  exti 1
}

# Verify tarball checksum GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE3}' --data='%{SOURCE2}'

# Verify tarball checksum (consistency)
[[ "$(sha256sum %{SOURCE0} | head -c 64)" == "$(head -c 64 %{SOURCE2})" ]] || {
  echo "Failed to verify the tarball checksum"
  exti 1
}

# Verify tarball GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE1}' --data='%{SOURCE0}'

# Run setup macro
%setup -q

%build

%install
# make sure everything runs successfully
set -euo pipefail

# create the required directory structure
mkdir -p %{buildroot}/%{_bindir}

# install package files
install -Z -m 755 bin/%{name} \
        %{buildroot}/%{_bindir}/%{name}

%files
%{_bindir}/%{name}
%license LICENSE
%doc README.md

%changelog
* Fri Apr 22 2022 Jarryd Lisher <jarryd@jlisher.com> - 0.1.0-1
- Minor bug fixes and cleanup
* Sat Apr 16 2022 Jarryd Lisher <jarryd@jlisher.com> - 0.0.4-1
- Initial release
